$(document).ready(function(){
  $("#boton").click(function(){
    var num = new CCustomNumber($("#numero").val());
    $("#inicial").hide();
    $("#validacion").hide();
    
    $("#sm8").text("Magnitud y signo 8 bits: " + num.toSignMagnitude(8));
    $("#sm16").text("Magnitud y signo 16 bits: " + num.toSignMagnitude(16));
    $("#sm32").text("Magnitud y signo 32 bits: " + num.toSignMagnitude(32));
    
    $("#excess8").html("Exceso 2<span class='superindice'>n-1</span> 8 bits: " + num.toExcess(8));
    $("#excess16").html("Exceso 2<span class='superindice'>n-1</span> 16 bits: " + num.toExcess(16));
    $("#excess32").html("Exceso 2<span class='superindice'>n-1</span> 32 bits: " + num.toExcess(32));
    
    $("#final").show();
    
    $(document).attr("title", "Coneversión M-S y exceso 2^(n-1)");
  });
  
  $("#reload").click(function(){
      window.location.reload();
  });
});


class CNumber {

    constructor(number)
    {
        this.number = number;
        console.log("number: " + parseInt(number));
        this.regexps = {
          10 : /^([-+]?\d*)(\d*)?$/
        }
        var matches = this.number.match(this.regexps[10]);
        if (!matches) {
            this.number = NaN;
        }
    }
    
    toBinary() 
    {
        return this.changeBase(this.number, 10, 2);
    }
    
    inRangeForSignMagnitude(bits)
    {
        return ((-Math.pow(2,bits-1) + 1) <= this.number) && ((Math.pow(2,bits-1) - 1) >= this.number);
    }
    
    inRangeForExcess(bits)
    {
        return ((-Math.pow(2,bits-1)) <= this.number) && ((Math.pow(2,bits-1) - 1) >= this.number);
    }
    
    isNegative()
    {
        return parseInt(this.number) < 0;
    }
    
    getZeros(q)
    {
        console.log(q);
        var z = '';
        for (var i = 0; i < q; i++) {
            z += "0";
        }
        return z;
    }
    
    toSignMagnitude(bits)
    {       
        var num = Math.abs(this.number).toString();
        var binary = this.changeBase(num, 10, 2).toString();
        var bLen = binary.length;
        
        if (this.isNegative()) {
            return "1" + this.getZeros(bits - bLen - 1) + binary;
        }
        return this.getZeros(bits - bLen) + binary;
    }
    
    toExcess(bits) {
        var offset = Math.pow(2, bits - 1);
        var num = (parseInt(this.number) + offset).toString();
        var binary = this.changeBase(num, 10, 2).toString();
        var bLen = binary.length;
        return this.getZeros(bits - bLen) + binary;
    }

    // yeah, from http://coderstoolbox.net/number/
    changeBase(number, fromBase, toBase) 
    {
        var base = fromBase;
        var s = number;
        
        if (s == '') {
            return '';
        }
        // Allow 0x in front of hex numbers
        if (base == 16 && s.substr(0, 2) == '0x') {
            s = s.substr(2);
        }
        s = s.replace(/^ +| +$/g, '');

        var n;
        var matches = s.match(this.regexps[base]);
        if (!matches /*/^[-+]?\w*(\.\w*)?$/.test(s) */) {
            n = NaN;
        } else if (!matches[2] || matches[2].length < 2) {
            n = parseInt('0' + matches[1], base);
        } else {
            n = parseInt('0' + matches[1], base);
            n += (matches[1].substr(0, 1) == '-' ? -1 : +1) * parseInt(matches[2].substr(1), base) / Math.pow(base, matches[2].length - 1);
        }
        // FIXME: check for invalid characters, that are silently ignored by parseInt()
        var bases = [2, 8, 10, 16];
        var i = bases.indexOf(toBase);
        var output;
        if (isNaN(n)) {
            output = 'NAN';
        } else if (16.25.toString(16) == '10.4') {
            // Opera 9 does not support toString() for floats with base != 10
            output = n.toString(bases[i]);
        } else {
            output = (n > 0 ? Math.floor(n) : Math.ceil(n)).toString(bases[i]);
            if (n % 1) {
                output += '.' + Math.round((Math.abs(n) % 1) * Math.pow(bases[i], 8)).toString(bases[i]);
                output = output.replace(/0+$/, '');
            }
        }
        return output.toUpperCase();
    }

} 

class CCustomNumber extends CNumber
{
    toSignMagnitude(bits)
    {
        if (isNaN(this.number)) {
            return "el input no es un número entero";
        }
        if (this.inRangeForSignMagnitude(bits)) {
            return super.toSignMagnitude(bits);
        } else {
            return "fuera de rango";
        }
    }
    
    toExcess(bits)
    {
        if (isNaN(this.number)) {
            return "el input no es un número entero";
        }
        if (this.inRangeForExcess(bits)) {
            return super.toExcess(bits);
        } else {
            return "fuera de rango";
        }
    }
}